from mccdaqR import DaqR_usb1608G
import matplotlib.pyplot as plt, numpy as np
from scipy.fftpack import fft
from toolsR import SoundR

if __name__ == '__main__':

    # Create the object daq and the Sound
    daq = DaqR_usb1608G()

    # Generate your sound arrays
    toneDuration = 5  # sec
    amplitude = 1.
    FsOut = 192000  # sample rate, depend on the sound card
    tvec = np.linspace(0, toneDuration, toneDuration * FsOut)
    fs = 10000
    s1 = amplitude * np.sin(2 * np.pi * fs * tvec)  # sound vector
    s2 = np.zeros(s1.size)  # empty sound

    # Create the sound server
    soundStream = SoundR(sampleRate=FsOut, deviceOut=8, channelsOut=2)

    # Print information
    print('wMaxPacketSize:', daq.getMaxPacketSize())

    # Scanner the chennel
    c = 0 # channel
    gain = 10 # in Volt. 10V, 5V, 2V, 1V
    mode = 0 # 0 for single endian, 1 for differential
    freq = 500000 # Hz
    t = 1 # seconds

    soundStream.load(s1)
    soundStream.playSound()
    print('Scan...')
    vecOut, vecTime = daq.scan(channel=c, g=gain, m=mode, timeScan=t, frequency=freq)
    soundStream.stopSound()
    print('End scan. Plot..')

    # Fourier Transform
    N = len(vecOut)
    yf = fft(vecOut)

    # Plot results
    plt.figure(1)
    plt.subplot(211)
    plt.plot(vecTime, vecOut)
    plt.xlabel('Time')
    plt.ylabel('Volt')
    plt.title('Example USB-1608G')

    plt.subplot(212)
    plt.plot(2.0/N * np.abs(yf[0:N//2]))
    plt.xlabel('Frequency')
    plt.ylabel('Module')
    plt.title('FFT')
    plt.show()






