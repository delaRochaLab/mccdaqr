
#define USB_1608G_H ...
#define USB1608G_PID         272
#define USB1608GX_PID        273
#define USB1608GX_2AO_PID    274

#define USB1608G_V2_PID      308
#define USB1608GX_V2_PID     309
#define USB1608GX_2AO_V2_PID 310

/* Counter Timer */
#define COUNTER0         0     //  Counter 0
#define COUNTER1         1     //  Counter 1

/* Aanalog Input */
#define SINGLE_ENDED   0
#define DIFFERENTIAL   1
#define CALIBRATION    3
#define LAST_CHANNEL   128
#define PACKET_SIZE    512       // max bulk transfer size in bytes
#define CONTINUOUS     1         // continuous input mode
  
/* Ananlog Output Scan Options */
#define AO_CHAN0       1   // Include Channel 0 in output scan
#define AO_CHAN1       2   // Include Channel 1 in output scan
#define AO_TRIG        16  // Use Trigger
#define AO_RETRIG_MODE 32  // Retrigger Mode
  
/* Ranges */
#define BP_10V 0      // +/- 10 V
#define BP_5V  1      // +/- 5V
#define BP_2V  2      // +/- 2V
#define BP_1V  3      // +/- 1V
  
/* Status bit values */
#define AIN_SCAN_RUNNING   2
#define AIN_SCAN_OVERRUN   4
#define AOUT_SCAN_RUNNING  8
#define AOUT_SCAN_UNDERRUN 16
#define AIN_SCAN_DONE      32
#define AOUT_SCAN_DONE     64
#define FPGA_CONFIGURED    128
#define FPGA_CONFIG_MODE   256

#define NCHAN_1608G          16  // max number of A/D channels in the device
#define NGAINS_1608G          4  // max number of gain levels
#define NCHAN_AO_1608GX       2  // number of analog output channels
#define MAX_PACKET_SIZE_HS  512  // max packet size for HS device
#define MAX_PACKET_SIZE_FS   64  // max packet size for FS device

typedef struct timerParams_t {
  uint32_t period;
  uint32_t pulseWidth;
  uint32_t count;
  uint32_t delay;
} timerParams;

typedef struct ScanList_t {
  uint8_t mode;
  uint8_t range;
  uint8_t channel;
} ScanList;

typedef struct usbDevice1608G_t {
  libusb_device_handle *udev;         // libusb 1.0 handle
  float table_AIn[NGAINS_1608G][2];   // calibration coefficients
  float table_AOut[NCHAN_AO_1608GX][2];
  ScanList list[NCHAN_1608G];
  uint8_t scan_list[NCHAN_1608G];     // scan list
  uint8_t options;
  int nChannels;
} usbDevice1608G;

/* function prototypes for the USB-1608G */
void usbCalDate_USB1608G(libusb_device_handle *udev, struct tm *date);
void usbDTristateW_USB1608G(libusb_device_handle *udev, uint16_t value);
uint16_t usbDTristateR_USB1608G(libusb_device_handle *udev);
uint16_t usbDPort_USB1608G(libusb_device_handle *udev);
void usbDLatchW_USB1608G(libusb_device_handle *udev, uint16_t value);
uint16_t usbDLatchR_USB1608G(libusb_device_handle *udev);
void usbBlink_USB1608G(libusb_device_handle *udev, uint8_t count);
void cleanup_USB1608G( libusb_device_handle *udev);
void usbTemperature_USB1608G(libusb_device_handle *udev, float *temperature);
void usbGetSerialNumber_USB1608G(libusb_device_handle *udev, char serial[9]);
void usbReset_USB1608G(libusb_device_handle *udev);
void usbFPGAConfig_USB1608G(libusb_device_handle *udev);
void usbFPGAData_USB1608G(libusb_device_handle *udev, uint8_t *data, uint8_t length);
void usbFPGAVersion_USB1608G(libusb_device_handle *udev, uint16_t *version);
uint16_t usbStatus_USB1608G(libusb_device_handle *udev);
void usbInit_1608G(libusb_device_handle *udev, int version);
void usbCounterInit_USB1608G(libusb_device_handle *udev, uint8_t counter);
uint32_t usbCounter_USB1608G(libusb_device_handle *udev, uint8_t counter);
void usbTimerControlR_USB1608G(libusb_device_handle *udev, uint8_t *control);
void usbTimerControlW_USB1608G(libusb_device_handle *udev, uint8_t control);
void usbTimerPeriodR_USB1608G(libusb_device_handle *udev, uint32_t *period);
void usbTimerPeriodW_USB1608G(libusb_device_handle *udev, uint32_t period);
void usbTimerPulseWidthR_USB1608G(libusb_device_handle *udev, uint32_t *pulseWidth);
void usbTimerPulseWidthW_USB1608G(libusb_device_handle *udev, uint32_t pulseWidth);
void usbTimerCountR_USB1608G(libusb_device_handle *udev, uint32_t *count);
void usbTimerCountW_USB1608G(libusb_device_handle *udev, uint32_t count);
void usbTimerDelayR_USB1608G(libusb_device_handle *udev, uint32_t *delay);
void usbTimerDelayW_USB1608G(libusb_device_handle *udev, uint32_t delay);
void usbTimerParamsR_USB1608G(libusb_device_handle *udev, timerParams *params);
void usbTimerParamsW_USB1608G(libusb_device_handle *udev, timerParams *params);
void usbMemoryR_USB1608G(libusb_device_handle *udev, uint8_t *data, uint16_t length);
void usbMemoryW_USB1608G(libusb_device_handle *udev, uint8_t *data, uint16_t length);
void usbMemAddressR_USB1608G(libusb_device_handle *udev, uint16_t address);
void usbMemAddressW_USB1608G(libusb_device_handle *udev, uint16_t address);
void usbMemWriteEnable_USB1608G(libusb_device_handle *udev);
void usbTriggerConfig_USB1608G(libusb_device_handle *udev, uint8_t options);
void usbTriggerConfigR_USB1608G(libusb_device_handle *udev, uint8_t *options);
uint16_t usbAIn_USB1608G(libusb_device_handle *udev, uint16_t channel);
void usbAInScanStart_USB1608G(libusb_device_handle *udev, uint32_t count, uint32_t retrig_count, double frequency, uint8_t options);
void usbAInScanStop_USB1608G(libusb_device_handle *udev);
int usbAInScanRead_USB1608G(libusb_device_handle *udev, int nScan, int nChan, uint16_t *data, unsigned int timeout, int options);
void usbAInConfig_USB1608G(libusb_device_handle *udev, ScanList scanList[NCHAN_1608G]);
int usbAInConfigR_USB1608G(libusb_device_handle *udev, uint8_t *scanList);
void usbAInScanClearFIFO_USB1608G(libusb_device_handle *udev);
void usbBuildGainTable_USB1608G(libusb_device_handle *udev, float table[NGAINS_1608G][2]);
double volts_USB1608G(const uint8_t gain, uint16_t value);
void usbBuildGainTable_USB1608GX_2AO(libusb_device_handle *udev, float table_AO[NCHAN_AO_1608GX][2]);
uint16_t voltsTou16_USB1608GX_AO(double volts, int channel, float table_AO[NCHAN_AO_1608GX][2]);
void usbAOut_USB1608GX_2AO(libusb_device_handle *udev, uint8_t channel, double voltage, float table_AO[NCHAN_AO_1608GX][2]);
void usbAOutR_USB1608GX_2AO(libusb_device_handle *udev, uint8_t channel, double *voltage, float table_AO[NCHAN_AO_1608GX][2]);
void usbAOutScanStop_USB1608GX_2AO(libusb_device_handle *udev);
void usbAOutScanClearFIFO_USB1608GX_2AO(libusb_device_handle *udev);
void usbAOutScanStart_USB1608GX_2AO(libusb_device_handle *udev, uint32_t count, uint32_t retrig_count, double frequency, uint8_t options);
